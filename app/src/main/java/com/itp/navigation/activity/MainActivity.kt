package com.itp.navigation.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.itp.navigation.R

private const val LAYOUT_ID = R.layout.activity_main

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(LAYOUT_ID)
    }
}