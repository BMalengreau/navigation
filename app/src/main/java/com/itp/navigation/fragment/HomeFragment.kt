package com.itp.navigation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.itp.navigation.R

private const val LAYOUT_ID = R.layout.fragment_home

class HomeFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(LAYOUT_ID, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val navigationController = Navigation.findNavController(view)

        view.findViewById<Button>(R.id.helloWorldButton).setOnClickListener {
            navigationController.navigate(R.id.showHelloWorldAction)
        }
    }
}